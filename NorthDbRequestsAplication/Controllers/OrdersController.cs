﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.UpdateModels;
using NorthDbRequestsAplication.Core.Abstractions;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Abstractions.Operations;
using Microsoft.AspNetCore.Authorization;

namespace NorthDbRequestsAplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderBL _orderBL;
        public OrdersController(IOrderBL orderBL)
        {
            _orderBL = orderBL;
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult AddOrder([FromBody] OrderUpdateModel orderUpdateModel)
        {
            var addedOrder = _orderBL.AddOrder(orderUpdateModel);
            return Created("", addedOrder);
        }
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult EditOrder([FromRoute] int id, [FromBody] OrderUpdateModel orderUpdateModel)
        {
            _orderBL.EditOrder(id, orderUpdateModel);
            return Ok(orderUpdateModel);
        }
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult RemoveOrder([FromRoute] int id)
        {
            _orderBL.RemoveOrder(id);
            return Ok();
        }

        [HttpGet]
        public IActionResult GetOrders([FromQuery] OrderFilterModel orderFilterModel)
        {
            var orders = _orderBL.GetOrders(orderFilterModel);
            return Ok(orders);
        }

        [HttpGet("highestFreigth/byCountry")]
        [Authorize]
        public IActionResult GetOrdersHighestFreithByCountry([FromQuery] OrderFilterModel orderFilterModel)
        {
            var ordersHighestFreithByCountry = _orderBL.GetOrdersHighestFreithByCountry(orderFilterModel);
            return Ok(ordersHighestFreithByCountry);
        }

        [HttpGet("sort/byEmployeeAndProduct")]
        [Authorize]
        public IActionResult GetSortedOrdersbyEmployeeAndProduct()
        {
            var sortedOrdersbyEmployeeAndProduct = _orderBL.GetSortedOrdersbyEmployeeAndProduct();
            return Ok(sortedOrdersbyEmployeeAndProduct);
        }

        [HttpGet("endMonth")]
        [Authorize]
        public IActionResult GetOrdersEndMonth()
        {
            var result = _orderBL.GetOrdersEndMonth();
            return Ok(result);
        }

        [HttpGet("withManyItems")]
        [Authorize]
        public IActionResult GetOrdersWithManyLineItems([FromQuery] OrderFilterModel orderFilterModel)
        {
            var result = _orderBL.GetOrdersWithManyLineItems(orderFilterModel);
            return Ok(result);
        }

        [HttpGet("randomAssortment")]
        [Authorize]
        public IActionResult GetOrdersRandomAssortment([FromQuery] OrderFilterModel orderFilterModel)
        {
            var result = _orderBL.GetOrdersRandomAssortment(orderFilterModel);
            return Ok(result);
        }


    }
}

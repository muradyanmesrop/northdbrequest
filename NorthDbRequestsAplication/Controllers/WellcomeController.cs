﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthDbRequestsAplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WellcomeController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetWellcome()
        {
            string wellcome = "Wellcome in my website. What do you want to request?";
            return Ok(wellcome);
        }
    }
}

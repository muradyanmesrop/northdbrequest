﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NorthDbRequestsAplication.Core.Abstractions.Operations;
using NorthDbRequestsAplication.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthDbRequestsAplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserBL _userBL;

        public UsersController(IUserBL userBL)
        {
            _userBL = userBL;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] UserRegisterModel registerModel)
        {
            var user = await _userBL.RegisterAsync(registerModel, HttpContext);
            return Created("", user);
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody] UserLoginModel loginModel)
        {
            await _userBL.LoginAsync(loginModel, HttpContext);
            return Ok();
        }

        [HttpPost("logout")]
        public async Task<IActionResult> LogOutAsync()
        {
            await _userBL.LogOutAsync(HttpContext);
            return Ok();
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditRole([FromRoute] int id, [FromBody] UserUpdateRoleModel userUpdateRoleModel)
        {
            await _userBL.EditRoleAsync(id, userUpdateRoleModel);
            return Ok();
        }
    }
}

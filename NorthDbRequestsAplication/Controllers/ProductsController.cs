﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NorthDbRequestsAplication.Core.Abstractions;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.UpdateModels;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Abstractions.Operations;
using Microsoft.AspNetCore.Authorization;

namespace NorthDbRequestsAplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductBL _productBL;

        public ProductsController(IProductBL productBL)
        {
            _productBL = productBL;
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult AddProduct([FromBody] ProductUpdateModel productUpdateModel)
        {
            var createdProduct = _productBL.AddProduct(productUpdateModel);
            return Created("", createdProduct);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult EditProduct([FromRoute] int id, [FromBody] ProductUpdateModel productUpdateModel)
        {
            _productBL.EditProduct(id, productUpdateModel);
            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult RemoveProduct([FromRoute] int id)
        {
            _productBL.RemoveProduct(id);
            return Ok();
        }

        [HttpGet]
        public IActionResult GetProducts([FromQuery] ProductFilterModel productFilterModel)
        {
            var prosucts = _productBL.GetProducts(productFilterModel);
            return Ok(prosucts);
        }

        [HttpGet("count/byCategory")]
        [Authorize]
        public IActionResult GetCountProductsByCategory()
        {
            var productsCountByCategory = _productBL.GetCountProductsByCategory();
            return Ok(productsCountByCategory);
        }

        [HttpGet("units/lessReorder")]
        [Authorize]
        public IActionResult GetProductsUnitsLessReorder()
        {
            var productsUnitsLessReorder = _productBL.GetProductsUnitsLessReorder();
            return Ok(productsUnitsLessReorder);
        }

        [HttpGet("units/needReordering")]
        [Authorize]
        public IActionResult GetReorderingProducts()
        {
            var productsUnitsLessReorder = _productBL.GetReorderingProducts();
            return Ok(productsUnitsLessReorder);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NorthDbRequestsAplication.Core.Abstractions;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.UpdateModels;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Abstractions.Operations;
using Microsoft.AspNetCore.Authorization;

namespace NorthDbRequestsAplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerBL _customerBL;

        public CustomersController(ICustomerBL customerBL)
        {
            _customerBL = customerBL;
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult AddCustomer([FromBody] CustomerUpdateModel customerUpdateModel)
        {
            var addedCustomer = _customerBL.AddCustomer(customerUpdateModel);
            return Created("", addedCustomer);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult EditCutomer([FromRoute] string id, [FromBody] CustomerUpdateModel customerUpdateModel)
        {
            _customerBL.EditCustomer(id, customerUpdateModel);
            return Ok(customerUpdateModel);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult RemoveCustomer([FromRoute] string id)
        {
            _customerBL.RemoveCustomer(id);
            return Ok();
        }

        [HttpGet]
        public IActionResult GetCustumers([FromQuery] CustomerFilterModel customerFilterModel)
        {
            var custumers = _customerBL.GetCustomers(customerFilterModel);
            return Ok(custumers);
        }

        [HttpGet("countByCountryAndCity")]
        [Authorize]
        public IActionResult GetCustumersCountByCountryAndCity()
        {
            var custumersCountByCountryAndCity = _customerBL.GetCustumersCountByCountryAndCity();
            return Ok(custumersCountByCountryAndCity);
        }
        [Authorize]
        [HttpGet("sort/byRegion")]
        public IActionResult GetSortedByRegionCustumers()
        {
            var sortedCustumersByRegion = _customerBL.GetSortedByRegionCustumers();
            return Ok(sortedCustumersByRegion);
        }

        [HttpGet("nonOrders")]
        [Authorize]
        public IActionResult GetCustumersNonOrders()
        {
            var nonOrderCustumers = _customerBL.GetCustumersNonOrders();
            return Ok(nonOrderCustumers);
        }

        [HttpGet("nonOrders/byEmployees/{id}")]
        [Authorize]
        public IActionResult GetCustumersNonOrdersbyEmployees(int id)
        {
            var nonOrderCustumersbyEmployees = _customerBL.GetCustumersNonOrdersbyEmployees(id);
            return Ok(nonOrderCustumersbyEmployees);
        }

        [HttpGet("sort/byHighValue")]
        [Authorize]
        public IActionResult GetSortCustumersbyHighValue()
        {
            var sortcustumersbyTotalUnitPrice = _customerBL.GetSortCustumersbyHighValue();
            return Ok(sortcustumersbyTotalUnitPrice);
        }

        [HttpGet("sort/byHighValue/totalOrders")]
        [Authorize]
        public IActionResult GetSortCustumersbyHighValueTotalOrders()
        {
            var result = _customerBL.GetSortCustumersbyHighValueTotalOrders();
            return Ok(result);
        }

        [HttpGet("sort/byHighValue/totalOrders/withDiscount")]
        [Authorize]
        public IActionResult GetSortCustumersbyHighValueTotalOrdersWithDiscount()
        {
            var result = _customerBL.GetSortCustumersbyHighValueTotalOrdersWithDiscount();
            return Ok(result);
        }

        [HttpPost("testTransaction")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> TestTransactionAsync()
        {
            await _customerBL.TestTransactionAsync();
            return Ok();
        }
    }
}

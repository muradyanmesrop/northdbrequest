﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using NorthDbRequestsAplication.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.DAL
{
    public class SqlTransaction : ISqlTransaction
    {
        private readonly IDbContextTransaction _transaction;

        public SqlTransaction(DbContext context, System.Data.IsolationLevel level)
        {
            _transaction = context.Database.BeginTransaction(level);
        }

        public static ISqlTransaction Begin(DbContext context, System.Data.IsolationLevel level)
        {
            return new SqlTransaction(context, level);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _transaction.Dispose();
            }
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public void RollBack()
        {
            _transaction.Rollback();
        }       
    }
}

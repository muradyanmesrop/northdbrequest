﻿using NorthDbRequestsAplication.Core.Abstractions.Repositories;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace NorthDbRequestsAplication.DAL.Repositories
{
    public class UserRepository : SqlRepositoryBase<User>, IUserRepository
    {
        public UserRepository(NORTHWNDContext db)
           : base(db)
        {
        }
    }
}

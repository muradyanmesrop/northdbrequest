﻿using NorthDbRequestsAplication.Core.Abstractions.Repositories;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthDbRequestsAplication.DAL.Repositories
{
    public class ProductRepository : SqlRepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(NORTHWNDContext nORTHWND)
            : base(nORTHWND)
        {

        }

        public IEnumerable<ProductsCountViewModel> GetCountProductsByCategory()
        {
            var result = _db.Products.Join(_db.Categories, x => x.CategoryId, y => y.CategoryId, (x, y) => new { y.CategoryName })
                                     .GroupBy(x => x.CategoryName)
                                     .Select(x => new { x.Key, ProductCount = x.Count() })
                                     .OrderByDescending(x => x.ProductCount)
                                     .Select(x=>new ProductsCountViewModel
                                     {
                                         CategoryName = x.Key,
                                         ProductCount = x.ProductCount
                                     })
                                     .ToList();
            return result;
        }

        public IEnumerable<ProductViewModel> GetProductsUnitsLessReorder()
        {
            var result = _db.Products.Where(x => x.UnitsInStock < x.ReorderLevel)
                                     .OrderBy(x => x.ProductId)
                                     .Select(x => new ProductViewModel(x))
                                     .ToList();
            return result;
        }

        public IEnumerable<ProductViewModel> GetReorderingProducts()
        {
            var result = _db.Products.Where(x => x.UnitsInStock + x.UnitsOnOrder <= x.ReorderLevel && x.Discontinued == false)
                                     .OrderBy(x => x.ProductId)
                                     .Select(x => new ProductViewModel(x))
                                     .ToArray();
            return result;
        }
    }
}

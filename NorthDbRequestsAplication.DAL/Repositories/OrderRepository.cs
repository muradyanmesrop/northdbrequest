﻿using NorthDbRequestsAplication.Core.Abstractions.Repositories;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthDbRequestsAplication.DAL.Repositories
{
    public class OrderRepository : SqlRepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(NORTHWNDContext nORTHWNDContext)
            : base(nORTHWNDContext)
        {

        }

        public IEnumerable<OrderViewModel> GetOrdersEndMonth()
        {
            var result = _db.Orders.Where(x => x.OrderDate.Value.Month != x.OrderDate.Value.AddDays(1).Month)                                         
                                         .OrderBy(x => x.EmployeeId)
                                         .ThenBy(x => x.OrderId)
                                         .Select(x => new OrderViewModel(x))
                                         .ToList();
            return result;
        }

        public IEnumerable<OrderHighestFreithViewModel> GetOrdersHighestFreith()
        {
            var result = _db.Orders.GroupBy(x => x.ShipCountry)
                                   .Select(x => new { x.Key, AverrageFreight = x.Average(x => x.Freight) })
                                   .OrderByDescending(x => x.AverrageFreight)
                                   .Select(x=> new OrderHighestFreithViewModel
                                   {
                                       ShipCountry = x.Key,
                                       AverrageFreight = x.AverrageFreight
                                   })
                                   .ToList();
            return result;
        }

        public IEnumerable<OrderHighestFreithViewModel> GetOrdersHighestFreithByYear(OrderFilterModel orderFilterModel)
        {
            var result = _db.Orders.Where(x => x.OrderDate.Value.Year == orderFilterModel.OrderYear)
                                   .GroupBy(x => x.ShipCountry)
                                   .Select(x => new { x.Key, AverrageFreight = x.Average(x => x.Freight) })
                                   .OrderByDescending(x => x.AverrageFreight)
                                   .AsQueryable();

            if (orderFilterModel.TopCount.HasValue)
            {
                result = result.Take(orderFilterModel.TopCount.Value).AsQueryable();
            }
            var viewResult = result.Select(x => new OrderHighestFreithViewModel
            {
                ShipCountry = x.Key,
                AverrageFreight = x.AverrageFreight
            })
            .ToList();
            return viewResult;
        }

        public IEnumerable<OrderHighestFreithViewModel> GetOrdersHighestFreithLastYear(OrderFilterModel orderFilterModel)
        {
            
            DateTime maxDate = _db.Orders.Max(x => x.OrderDate).Value;
            DateTime lastMothsDate = maxDate.AddMonths(-(int)orderFilterModel.LastMonthCount);
            var result = _db.Orders.Where(x => x.OrderDate <= maxDate && x.OrderDate >= lastMothsDate)
                                   .GroupBy(x => x.ShipCountry)
                                   .Select(x => new { x.Key, AverrageFreight = x.Average(x => x.Freight) })
                                   .OrderByDescending(x => x.AverrageFreight)
                                   .AsQueryable();

            if (orderFilterModel.TopCount.HasValue)
            {
                result = result.Take(orderFilterModel.TopCount.Value);
            }

            var viewResult = result.Select(x => new OrderHighestFreithViewModel
            {
                ShipCountry = x.Key,
                AverrageFreight = x.AverrageFreight
            })
            .ToList();

            return viewResult;

        }

        public IEnumerable<OrderViewModel> GetOrdersRandomAssortment(int? count)
        {
            var result = _db.Orders.OrderBy(x => Guid.NewGuid()).AsQueryable();
            
            if (count.HasValue)
            {
                result = result.Take(count.Value).AsQueryable();
            }

            var viewResult = result.Select(x => new OrderViewModel(x)).ToList();

            return viewResult;
        }

        public IEnumerable<OrderCountItemViewModel> GetOrdersWithManyLineItems(int? count)
        {
            var result = _db.OrderDetails.GroupBy(x => x.OrderId)
                                         .Select(x => new { OrderId = x.Key, TotalOrderDetails = x.Count() })
                                         .OrderByDescending(x => x.TotalOrderDetails)
                                         .AsQueryable();
            if (count.HasValue)
            {
                result = result.Take(count.Value).AsQueryable();
            }
            var viewResult = result.Select(x => new OrderCountItemViewModel
            {
                OrderId = x.OrderId,
                TotalOrderDetails = x.TotalOrderDetails
            }).ToList();
            return viewResult;
        }

        public IEnumerable<OrderEmployeeAndProductViewModel> GetSortedOrdersbyEmployeeAndProduct()
        {
            var result = _db.Orders.Join(_db.Employees, x => x.EmployeeId, y => y.EmployeeId, (x, y) => new { y.EmployeeId, y.LastName, x.OrderId })
                                   .Join(_db.OrderDetails, x => x.OrderId, y => y.OrderId, (x, y) => new { x.EmployeeId, x.LastName, x.OrderId, y.ProductId, y.Quantity })
                                   .Join(_db.Products, x => x.ProductId, y => y.ProductId, (x, y) => new { x.EmployeeId, x.LastName, x.OrderId, x.ProductId, y.ProductName, x.Quantity })
                                   .OrderBy(x => x.OrderId)
                                   .ThenBy(y => y.ProductId)
                                   .Select(x => new OrderEmployeeAndProductViewModel
                                   {
                                       EmployeeId = x.EmployeeId,
                                       LastName = x.LastName,
                                       OrderId = x.OrderId,
                                       ProductName = x.ProductName,
                                       Quantity = x.Quantity
                                   })
                                   .ToList();

            return result;
        }
    }
}

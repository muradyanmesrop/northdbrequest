﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NorthDbRequestsAplication.Core.Abstractions.Repositories;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.ViewModels;

namespace NorthDbRequestsAplication.DAL.Repositories
{
    public class CustomerRepository : SqlRepositoryBase<Customer>, ICustomerRepository
    {
        public CustomerRepository(NORTHWNDContext nORTHWNDContext)
            : base(nORTHWNDContext)
        {

        }

        public IEnumerable<CustomersCountViewModel> GetCustumersCountByCountryAndCity()
        {
            var result = _db.Customers.GroupBy(x => new { x.Country, x.City })
                                      .Select(x => new CustomersCountViewModel { Country = x.Key.Country, City = x.Key.City, CutomersCount = x.Count() })
                                      .OrderByDescending(x => x.CutomersCount)
                                      .ToList();
            return result;
        }

        public IEnumerable<CustomerViewModel> GetCustumersNonOrders()
        {
            var result = (from customer in _db.Customers
                          join order in _db.Orders on customer.CustomerId equals order.CustomerId into gj
                          from subOrder in gj.DefaultIfEmpty()
                          select new { customer, NonID = (customer.CustomerId == subOrder.CustomerId) ? subOrder.CustomerId : null } into subNew
                          where subNew.NonID == null
                          select new CustomerViewModel(subNew.customer))
                          .ToList();
            return result;
        }

        public IEnumerable<CustomerViewModel> GetCustumersNonOrdersbyEmployees(int id)
        {
            var result = (from customer in _db.Customers
                          join order in _db.Orders.Where(x => x.EmployeeId == id) on customer.CustomerId equals order.CustomerId into gj
                          from subOrder in gj.DefaultIfEmpty()
                          select new { customer, NonID = (customer.CustomerId == subOrder.CustomerId) ? subOrder.CustomerId : null } into subNew
                          where subNew.NonID == null
                          select new CustomerViewModel(subNew.customer))
                          .ToList();
            return result;
        }

        public IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValue()
        {
            var result = _db.OrderDetails.Join(_db.Orders.Where(x => x.OrderDate.Value.Year == 1998),
                                                x => x.OrderId,
                                                y => y.OrderId,
                                                (x, y) => new { x.OrderId, x.Quantity, x.UnitPrice, y.CustomerId })
                                         .Join(_db.Customers,
                                               x => x.CustomerId,
                                               y => y.CustomerId,
                                               (x, y) => new { y.CustomerId, y.CompanyName, x.OrderId, x.Quantity, x.UnitPrice })
                                         .GroupBy(x => new { x.CustomerId, x.CompanyName, x.OrderId })
                                         .Where(x => x.Sum(x => x.Quantity * x.UnitPrice) >= 10000)
                                         .Select(x => new CustomerHighValueViewModel
                                                {
                                                    CustomerId = x.Key.CustomerId,
                                                    CompanyName = x.Key.CompanyName,                                                    
                                                    TotalsWithoutDiscount = x.Sum(x => x.Quantity * x.UnitPrice)
                                                })
                                         .OrderByDescending(x => x.TotalsWithoutDiscount)
                                         .ToList();
            return result;
        }

        public IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValueTotalOrders()
        {
            var result = (from orderDetails in _db.OrderDetails
                          join order in _db.Orders.Where(x => x.OrderDate.Value.Year == 1998) on orderDetails.OrderId equals order.OrderId
                          join customer in _db.Customers on order.CustomerId equals customer.CustomerId
                          group new { orderDetails, order, customer } by new { customer.CustomerId, customer.CompanyName } into newTable
                          where newTable.Sum(x => x.orderDetails.UnitPrice * x.orderDetails.Quantity) >= 15000
                          orderby newTable.Sum(x => x.orderDetails.UnitPrice * x.orderDetails.Quantity) descending
                          select new CustomerHighValueViewModel
                          {
                              CustomerId = newTable.Key.CustomerId,
                              CompanyName = newTable.Key.CompanyName,
                              TotalsWithoutDiscount = newTable.Sum(x => x.orderDetails.UnitPrice * x.orderDetails.Quantity)
                          })
                          .ToList();

            return result;
        }

        public IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValueTotalOrdersWithDiscount()
        {
            var result = (from orderDetails in _db.OrderDetails
                          join order in _db.Orders.Where(x => x.OrderDate.Value.Year == 1998) on orderDetails.OrderId equals order.OrderId
                          join customer in _db.Customers on order.CustomerId equals customer.CustomerId
                          group new
                          {
                              TotalsWithoutDiscount = orderDetails.Quantity * orderDetails.UnitPrice,
                              TotalsWithDiscount = orderDetails.Quantity * orderDetails.UnitPrice * (decimal)(1 - orderDetails.Discount),
                              customer.CustomerId,
                              customer.CompanyName
                          }
                          by new { customer.CustomerId, customer.CompanyName } into newTable
                          where newTable.Sum(x => x.TotalsWithDiscount) >= 10000
                          orderby newTable.Sum(x => x.TotalsWithDiscount) descending
                          select new CustomerHighValueViewModel
                          {
                              CustomerId = newTable.Key.CustomerId,
                              CompanyName = newTable.Key.CompanyName,
                              TotalsWithoutDiscount = newTable.Sum(x => x.TotalsWithoutDiscount),
                              TotalsWithDiscount = newTable.Sum(x => x.TotalsWithDiscount)
                          })
                          .ToList();

            return result;
        }

        public IEnumerable<Customer> GetSortedByRegionCustumers()
        {
            var temp = _db.Customers
              .OrderBy(x => x.Region)
              .ThenBy(x => x.CustomerId)              
              .ToList();

            var temp1 = temp.Where(x => x.Region != null).ToList();
            var temp2 = temp.Where(x => x.Region == null).ToList();

            foreach (var item in temp2)
            {
                temp1.Add(item);
            }
            var result = temp1.ToList();
            return result;
        }
    }
}

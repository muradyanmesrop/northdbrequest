﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NorthDbRequestsAplication.Core.Abstractions.Repositories;
using NorthDbRequestsAplication.DAL;

namespace NorthDbRequestsAplication.DAL.Repositories
{
    public class SqlRepositoryBase<T> : ISqlRepository<T>
        where T : class
    {
        protected internal readonly NORTHWNDContext _db;

        public SqlRepositoryBase(NORTHWNDContext nORTHWNDContext)
        {
            _db = nORTHWNDContext;
        }
        public T Add(T entity)
        {
            _db.Set<T>().Add(entity);
            return entity;
        }

        public T Get(int id)
        {
            return _db.Set<T>().Find(id);
        }

        public IEnumerable<T> GetWhere(Func<T, bool> predicate)
        {
            return _db.Set<T>().Where(predicate);
        }

        public void Remove(T entity)
        {
            _db.Set<T>().Remove(entity);
        }

        public int SaveChanges()
        {
            return _db.SaveChanges();
        }

        public void Edit (T entity)
        {
            _db.Set<T>().Update(entity);
        }

        public int Count()
        {
            return _db.Set<T>().Count();
        }

        public T Get(string id)
        {
            return _db.Set<T>().Find(id);
        }
    }
}

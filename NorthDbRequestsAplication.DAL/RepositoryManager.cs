﻿using NorthDbRequestsAplication.Core.Abstractions;
using NorthDbRequestsAplication.Core.Abstractions.Repositories;
using NorthDbRequestsAplication.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace NorthDbRequestsAplication.DAL
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly NORTHWNDContext _db;

        public RepositoryManager(NORTHWNDContext nORTHWNDContext)
        {
            _db = nORTHWNDContext;
        }

        private ICustomerRepository _customers;
        public ICustomerRepository Customers => _customers ?? (_customers = new CustomerRepository(_db));

        private IOrderRepository _orders;
        public IOrderRepository Orders => _orders ?? (_orders = new OrderRepository(_db));

        private IProductRepository _products;
        public IProductRepository Products => _products ?? (_products = new ProductRepository(_db));

        private IUserRepository _users;
        public IUserRepository Users => _users ?? (_users = new UserRepository(_db));

        public ISqlTransaction BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return SqlTransaction.Begin(_db, isolationLevel);
        }

        public int SaveChanges()
        {
            return _db.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _db.SaveChangesAsync();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NorthDbRequestsAplication.Core.Abstractions.Operations;
using NorthDbRequestsAplication.Core.Abstractions.Repositories;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.UpdateModels;
using NorthDbRequestsAplication.Core.Abstractions;
using NorthDbRequestsAplication.Core.Exceptions;
using NorthDbRequestsAplication;
using Microsoft.Extensions.Logging;
using NorthDbRequestsAplication.Core.Models.ViewModels;

namespace NorthDbRequestsAplication.BLL.Operations
{
    public class OrderBL : IOrderBL
    {
        private readonly IRepositoryManager _repositories;
        private readonly ILogger<OrderBL> _logger;

        public OrderBL(IRepositoryManager repositories, ILogger<OrderBL> logger)
        {
            _logger = logger;
            _repositories = repositories;
        }

        public OrderViewModel AddOrder(OrderUpdateModel orderUpdateModel)
        {
            _logger.LogInformation("AddOrder method started");
            if (orderUpdateModel == null)
            {
                throw new LogicException("Wrong Customer ID");
            }

            Order order = orderUpdateModel;

            _repositories.Orders.Add(order);
            _repositories.SaveChanges();

            _logger.LogInformation("AddOrder method finished");
            OrderViewModel orderView = order;
            return orderView;
        }

        public void EditOrder(int id, OrderUpdateModel orderUpdateModel)
        {
            _logger.LogInformation("EditOrder method started");
            var dborder = _repositories.Orders.Get(id);
            if (dborder == null || orderUpdateModel == null)
            {
                throw new LogicException("Wrong Order ID or there aren`t dataes");
            }

            Order order = orderUpdateModel;
            order.OrderId = dborder.OrderId;

            _repositories.Orders.Edit(order);
            _repositories.SaveChanges();

            _logger.LogInformation("EditOrder method finished");
        }

        public void RemoveOrder(int id)
        {
            _logger.LogInformation("RemoveOrder method started");
            var dbOrder = _repositories.Orders.Get(id);
            if (dbOrder == null)
            {
                throw new LogicException("Wrong Order ID");
            }

            _repositories.Orders.Remove(dbOrder);
            _repositories.Orders.SaveChanges();

            _logger.LogInformation("RemoveOrder method finished");
        }

        public IEnumerable<OrderViewModel> GetOrders(OrderFilterModel orderFilterModel)
        {
            List<OrderViewModel> orders = _repositories.Orders.GetWhere(x =>
                                                   (!orderFilterModel.OrderId.HasValue || x.OrderId == orderFilterModel.OrderId)
                                                   && (string.IsNullOrEmpty(orderFilterModel.CustomerId) || x.CustomerId == orderFilterModel.CustomerId)
                                                   &&(!orderFilterModel.EmployeeId.HasValue || x.EmployeeId == orderFilterModel.EmployeeId)
                                                   && (!orderFilterModel.OrderYear.HasValue || x.OrderDate.Value.Year == orderFilterModel.OrderYear)
                                                   && (!orderFilterModel.OrderMonth.HasValue || x.OrderDate.Value.Month == orderFilterModel.OrderMonth)
                                                   && (!orderFilterModel.RequiredYear.HasValue || x.RequiredDate.Value.Year == orderFilterModel.RequiredYear)
                                                   && (!orderFilterModel.RequiredMonth.HasValue || x.RequiredDate.Value.Month == orderFilterModel.RequiredMonth)
                                                   && (!orderFilterModel.ShippedYear.HasValue || x.ShippedDate.Value.Year == orderFilterModel.ShippedYear)
                                                   && (!orderFilterModel.ShippedMonth.HasValue || x.ShippedDate.Value.Month == orderFilterModel.ShippedMonth)
                                                   && (!orderFilterModel.ShipVia.HasValue || x.ShipVia == orderFilterModel.ShipVia)
                                                   && (!orderFilterModel.FreightLess.HasValue || x.Freight < orderFilterModel.FreightLess)
                                                   && (!orderFilterModel.FreightMore.HasValue || x.Freight > orderFilterModel.FreightMore)
                                                   && (string.IsNullOrEmpty(orderFilterModel.ShipName) || x.ShipName.ToUpper().Contains(orderFilterModel.ShipName.ToUpper()))
                                                   && (string.IsNullOrEmpty(orderFilterModel.ShipAddress) || x.ShipAddress.ToUpper().Contains(orderFilterModel.ShipAddress.ToUpper()))
                                                   && (string.IsNullOrEmpty(orderFilterModel.ShipCity) || x.ShipCity.ToUpper().Contains(orderFilterModel.ShipCity.ToUpper()))
                                                   && (string.IsNullOrEmpty(orderFilterModel.ShipRegion) || x.ShipRegion.ToUpper().Contains(orderFilterModel.ShipRegion.ToUpper()))
                                                   && (string.IsNullOrEmpty(orderFilterModel.ShipPostalCode) || x.ShipPostalCode.ToUpper().Contains(orderFilterModel.ShipPostalCode.ToUpper()))
                                                   && (string.IsNullOrEmpty(orderFilterModel.ShipCountry) || x.ShipCountry.ToUpper().Contains(orderFilterModel.ShipCountry.ToUpper())))
                                                   .Select(x=>new OrderViewModel(x))
                                                   .ToList();  

            return orders;
        }

        public IEnumerable<OrderHighestFreithViewModel> GetOrdersHighestFreithByCountry(OrderFilterModel orderFilterModel)
        {
            IEnumerable<OrderHighestFreithViewModel> result;

            if (orderFilterModel.OrderYear.HasValue)
            {
               result = _repositories.Orders.GetOrdersHighestFreithByYear(orderFilterModel);
            }
            else if (orderFilterModel.LastMonthCount.HasValue)
            {
                result = _repositories.Orders.GetOrdersHighestFreithLastYear(orderFilterModel);
            }
            else
            {
                result = _repositories.Orders.GetOrdersHighestFreith();
            }

            return result.ToList();
        }

        public IEnumerable<OrderEmployeeAndProductViewModel> GetSortedOrdersbyEmployeeAndProduct()
        {
            var result = _repositories.Orders.GetSortedOrdersbyEmployeeAndProduct();

            return result;
        }

        public IEnumerable<OrderViewModel> GetOrdersEndMonth()
        {
            var sortedValues = _repositories.Orders.GetOrdersEndMonth();
            return sortedValues;
        }

        public IEnumerable<OrderCountItemViewModel> GetOrdersWithManyLineItems(OrderFilterModel orderFilterModel)
        {
            IEnumerable<OrderCountItemViewModel> result;

            if (orderFilterModel.TopCount.HasValue)
            {
                result = _repositories.Orders.GetOrdersWithManyLineItems(orderFilterModel.TopCount.Value);
            }
            else
            {
                result = _repositories.Orders.GetOrdersWithManyLineItems(null);
            }
            return result;
        }

        public IEnumerable<OrderViewModel> GetOrdersRandomAssortment(OrderFilterModel orderFilterModel)
        {
            IEnumerable<OrderViewModel> result;
            if (orderFilterModel.TopCountPersent.HasValue)
            {
                int count = (int)Math.Round(_repositories.Orders.Count() * orderFilterModel.TopCountPersent.Value / 100.0);
                result = _repositories.Orders.GetOrdersRandomAssortment(count);
            }
            else
            {
                result = _repositories.Orders.GetOrdersRandomAssortment(null);
            }

            return result.ToList();
        }
    }
}

﻿using NorthDbRequestsAplication.Core.Abstractions;
using NorthDbRequestsAplication.Core.Abstractions.Operations;
using NorthDbRequestsAplication.Core.Models;
using NorthDbRequestsAplication.Core.Models.ViewModels;
using NorthDbRequestsAplication.Core.Db;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Linq;
using NorthDbRequestsAplication.Core.Exceptions;
using ServiceStack;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using NorthDbRequestsAplication.Core.Enums;

namespace NorthDbRequestsAplication.BLL.Operations
{
    public class UserBL : IUserBL
    {
        private readonly IRepositoryManager _repositories;
        public UserBL(IRepositoryManager repositories)
        {
            _repositories = repositories;
        }

        public async Task EditRoleAsync(int id, UserUpdateRoleModel userUpdateRoleModel)
        {
            var user = _repositories.Users.Get(id);
            user.Role = userUpdateRoleModel.Role;
            _repositories.Users.Edit(user);
            await _repositories.SaveChangesAsync();
        }

        public async Task LoginAsync(UserLoginModel loginModel, HttpContext httpContext)
        {
            var users = _repositories.Users.GetWhere(x => x.UserName == loginModel.Username && x.Password == loginModel.Password);
            if (users == null || !users.Any())
            {
                throw new LogicException("Wrong username or / and password");
            }

            await Authenticate(users.First(), httpContext);
        }

        public Task LogOutAsync(HttpContext httpContext)
        {
            return httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public async Task<UserViewModel> RegisterAsync(UserRegisterModel registerModel, HttpContext httpContext)
        {
            var users = _repositories.Users.GetWhere(x => x.UserName == registerModel.UserName);
            if (users.Any())
            {
                throw new LogicException("Username is already taken");
            }
            var user = new User
            {
                UserName = registerModel.UserName,
                Password = registerModel.Password,
                Role = Role.User
            };
            _repositories.Users.Add(user);
            await _repositories.SaveChangesAsync();
            await Authenticate(user, httpContext);
            return new UserViewModel
            {
                Id = user.Id,
                UserName = user.UserName,
            };
        }
        private async Task Authenticate(User user, HttpContext httpContext)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.ToString()),
                new Claim("Id",user.Id.ToString())
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NorthDbRequestsAplication.Core.Abstractions.Operations;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.UpdateModels;
using NorthDbRequestsAplication.Core.Abstractions;
using NorthDbRequestsAplication.Core.Exceptions;
using Microsoft.Extensions.Logging;
using NorthDbRequestsAplication.Core.Models.ViewModels;

namespace NorthDbRequestsAplication.BLL.Operations
{
    public class ProductBL : IProductBL
    {
        private readonly  IRepositoryManager _repositories;
        private readonly ILogger<ProductBL> _logger;

        public ProductBL(IRepositoryManager repositories, ILogger<ProductBL> logger)
        {
            _logger = logger;
            _repositories = repositories;
        }

        public ProductViewModel AddProduct(ProductUpdateModel productUpdateModel)
        {
            _logger.LogInformation("AddProduct method started");
            if (productUpdateModel == null)
            {
                throw new LogicException("There aren`t dataes");
            }

            Product product = productUpdateModel;

            _repositories.Products.Add(product);
            _repositories.SaveChanges();

            ProductViewModel productView = product;
            _logger.LogInformation("AddProduct method finished");
            return productView;
        }

        public void EditProduct(int id, ProductUpdateModel productUpdateModel)
        {
            _logger.LogInformation("EditProduct method started");
            var dbProduct = _repositories.Products.Get(id);
            if (dbProduct == null || productUpdateModel == null)
            {
                throw new LogicException("Wrong Product ID or there aren`t dataes");
            }

            Product product = productUpdateModel;
            product.ProductId = dbProduct.ProductId;

            _repositories.Products.Edit(product);
            _repositories.SaveChanges();
            _logger.LogInformation("EditProduct method finished");
        }

        public void RemoveProduct(int id)
        {
            _logger.LogInformation("RemoveProduct method started");
            var dbProduct = _repositories.Products.Get(id);
            if (dbProduct == null)
            {
                throw new LogicException("Wrong Product ID");
            }

            _repositories.Products.Remove(dbProduct);
            _repositories.Products.SaveChanges();
            _logger.LogInformation("RemoveProduct method finished");
        }

        public IEnumerable<ProductViewModel> GetProducts(ProductFilterModel productFilterModel)
        {
            List<ProductViewModel> products = _repositories.Products.GetWhere(x =>
                                                  (!productFilterModel.ProductId.HasValue || x.ProductId == productFilterModel.ProductId)
                                                  && (string.IsNullOrEmpty(productFilterModel.ProductName) || x.ProductName.ToUpper().Contains(productFilterModel.ProductName.ToUpper()))
                                                  && (!productFilterModel.CategoryId.HasValue || x.CategoryId == productFilterModel.CategoryId)
                                                  && (!productFilterModel.UnitPriceMore.HasValue || x.UnitPrice > productFilterModel.UnitPriceMore)
                                                  && (!productFilterModel.UnitPriceLess.HasValue || x.UnitPrice < productFilterModel.UnitPriceLess)
                                                  && (!productFilterModel.UnitsInStockMore.HasValue || x.UnitsInStock > productFilterModel.UnitsInStockMore)
                                                  && (!productFilterModel.UnitsInStockLess.HasValue || x.UnitsInStock < productFilterModel.UnitsInStockLess)
                                                  && (!productFilterModel.UnitsOnOrderMore.HasValue || x.UnitsOnOrder > productFilterModel.UnitsOnOrderMore)
                                                  && (!productFilterModel.UnitsOnOrderLess.HasValue || x.UnitsOnOrder < productFilterModel.UnitsOnOrderLess)
                                                  && (!productFilterModel.ReorderLevelMore.HasValue || x.ReorderLevel > productFilterModel.ReorderLevelMore)
                                                  && (!productFilterModel.ReorderLevelLess.HasValue || x.ReorderLevel < productFilterModel.ReorderLevelLess)
                                                  && (!productFilterModel.Discontinued.HasValue || x.Discontinued == productFilterModel.Discontinued))
                                                  .Select(x=> new ProductViewModel(x))
                                                  .ToList();            
         
            return products;
        }

        public IEnumerable<ProductsCountViewModel> GetCountProductsByCategory()
        {
            var countProductsByCategory = _repositories.Products.GetCountProductsByCategory();
            return countProductsByCategory;
        }

        public IEnumerable<ProductViewModel> GetProductsUnitsLessReorder()
        {
            var productsUnitsLessReorder = _repositories.Products.GetProductsUnitsLessReorder();
            return productsUnitsLessReorder;
        }

        public IEnumerable<ProductViewModel> GetReorderingProducts()
        {
            var productsUnitsLessReorder = _repositories.Products.GetReorderingProducts();
            return productsUnitsLessReorder;
        }
    }
}

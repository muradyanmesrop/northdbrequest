﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NorthDbRequestsAplication.Core.Abstractions;
using NorthDbRequestsAplication.Core.Abstractions.Operations;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.UpdateModels;
using NorthDbRequestsAplication.Core.Exceptions;
using Microsoft.Extensions.Logging;
using NorthDbRequestsAplication.Core.Models.ViewModels;

namespace NorthDbRequestsAplication.BLL.Operations
{
    public class CustomerBL : ICustomerBL
    {
        private readonly IRepositoryManager _repositories;
        private readonly ILogger<CustomerBL> _logger;

        public CustomerBL (IRepositoryManager repositories, ILogger<CustomerBL> logger)
        {
            _repositories = repositories;
            _logger = logger;
        }
        public CustomerViewModel AddCustomer(CustomerUpdateModel customerUpdateModel)
        {
            _logger.LogInformation("AddCustomer method started");
            if (customerUpdateModel == null)
            {
                throw new LogicException("There aren`t dataes");
            }
            
            Customer customer = customerUpdateModel;

            _repositories.Customers.Add(customer);
            _repositories.SaveChanges();

            CustomerViewModel customerViewModel = customer;

            _logger.LogInformation("AddCustomer method finished");            

            return customerViewModel;
        }

        public void EditCustomer(string id, CustomerUpdateModel customerUpdateModel)
        {
            _logger.LogInformation("EditCustomer method started");
            var dbCustomer = _repositories.Customers.Get(id);
            if (customerUpdateModel == null || dbCustomer == null)
            {
                throw new LogicException("Wrong Customer ID or there aren`t dataes");
            }

            Customer customer = customerUpdateModel;
            customer.CustomerId = dbCustomer.CustomerId;

            _repositories.Customers.Edit(customer);
            _repositories.SaveChanges();

            _logger.LogInformation("EditCustomer method finished");
        }
        public void RemoveCustomer(string id)
        {
            _logger.LogInformation("RemoveCustomer method started");
            var dbCustomer = _repositories.Customers.Get(id);
            if (dbCustomer == null)
            {
                throw new LogicException("Wrong Customer ID");
            }

            _repositories.Customers.Remove(dbCustomer);
            _repositories.SaveChanges();
            _logger.LogInformation("RemoveCustomer method finished");
        }

        public IEnumerable<CustomerViewModel> GetCustomers(CustomerFilterModel customerFilterModel)
        {
            List<CustomerViewModel> customers = _repositories.Customers.GetWhere(x =>
                                                           (string.IsNullOrEmpty(customerFilterModel.CustomerId) || x.CustomerId == customerFilterModel.CustomerId)
                                                           && (string.IsNullOrEmpty(customerFilterModel.Address) || x.Address.ToUpper().Contains(customerFilterModel.Address.ToUpper()))
                                                           && (string.IsNullOrEmpty(customerFilterModel.City) || x.City.ToUpper().Contains(customerFilterModel.City.ToUpper()))
                                                           && (string.IsNullOrEmpty(customerFilterModel.CompanyName) || x.CompanyName.ToUpper().Contains(customerFilterModel.CompanyName.ToUpper()))
                                                           && (string.IsNullOrEmpty(customerFilterModel.ContactName) || x.ContactName.ToUpper().Contains(customerFilterModel.ContactName.ToUpper()))
                                                           && (string.IsNullOrEmpty(customerFilterModel.ContactTitle) || x.ContactTitle.ToUpper().Contains(customerFilterModel.ContactTitle.ToUpper()))
                                                           && (string.IsNullOrEmpty(customerFilterModel.Country) || x.Country.ToUpper().Contains(customerFilterModel.Country.ToUpper()))
                                                           && (string.IsNullOrEmpty(customerFilterModel.Fax) || x.Fax.ToUpper().Contains(customerFilterModel.Fax.ToUpper()))
                                                           && (string.IsNullOrEmpty(customerFilterModel.Phone) || x.Phone.ToUpper().Contains(customerFilterModel.Phone.ToUpper()))
                                                           && (string.IsNullOrEmpty(customerFilterModel.Region) || x.Region.ToUpper().Contains(customerFilterModel.Region.ToUpper()))
                                                           ).Select(x => new CustomerViewModel(x))
                                                           .ToList();


            return customers;
        }

        public IEnumerable<CustomersCountViewModel> GetCustumersCountByCountryAndCity()
        {
            var custumersCountByCountryAndCity = _repositories.Customers.GetCustumersCountByCountryAndCity();
            return custumersCountByCountryAndCity;
        }

        public IEnumerable<CustomerViewModel> GetCustumersNonOrders()
        {
            var nonOrderCustumers = _repositories.Customers.GetCustumersNonOrders();
            return nonOrderCustumers;
        }

        public IEnumerable<CustomerViewModel> GetCustumersNonOrdersbyEmployees(int id)
        {
            var result = _repositories.Customers.GetCustumersNonOrdersbyEmployees(id);
            return result;
        }

        public IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValue()
        {
            var sortcustumersbyTotalUnitPrice = _repositories.Customers.GetSortCustumersbyHighValue();
            return sortcustumersbyTotalUnitPrice;
        }

        public IEnumerable<CustomerViewModel> GetSortedByRegionCustumers()
        {
            var result = _repositories.Customers.GetSortedByRegionCustumers().Select(x=>new CustomerViewModel(x));
            return result;
        }

        public IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValueTotalOrders()
        {
            var result = _repositories.Customers.GetSortCustumersbyHighValueTotalOrders();
            return result;
        }

        public IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValueTotalOrdersWithDiscount()
        {
            var result = _repositories.Customers.GetSortCustumersbyHighValueTotalOrdersWithDiscount();
            return result;
        }

        public async Task TestTransactionAsync()
        {
            using (var transaction = _repositories.BeginTransaction())
            {
                try
                {
                    var customer = new Customer
                    {
                        CustomerId = "PROMN",
                        CompanyName = "Name",
                        ContactName = "ContactName",
                        ContactTitle = "Title",
                        Address = "Adress",
                        City = "City",
                        Region = "Region",
                        PostalCode = "Code",
                        Country = "Country",
                        Phone = "Phone",
                        Fax = "Fax"
                    };
                    var product = new Product
                    {
                        ProductName = "Name",
                        SupplierId = 8,
                        CategoryId = 2,
                        QuantityPerUnit = "10 boxes x 20 bags",
                        UnitPrice = 45.3m,
                        UnitsInStock = 45,
                        UnitsOnOrder = 33,
                        ReorderLevel = 10,
                        Discontinued = true                       
                    };

                    _repositories.Customers.Add(customer);
                    //throw new Exception();
                    _repositories.Products.Add(product);
                    await _repositories.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.RollBack();
                    throw;
                }
            }
        }
    }
}

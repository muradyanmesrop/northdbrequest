﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Abstractions
{
    public interface ISqlTransaction : IDisposable
    {
        void Commit();
        void RollBack();
    }
}

﻿using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.UpdateModels;
using NorthDbRequestsAplication.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NorthDbRequestsAplication.Core.Abstractions.Operations
{
    public interface ICustomerBL
    {
        IEnumerable<CustomerViewModel> GetCustomers(CustomerFilterModel customerFilterModel);
        IEnumerable<CustomersCountViewModel> GetCustumersCountByCountryAndCity();
        IEnumerable<CustomerViewModel> GetSortedByRegionCustumers();
        IEnumerable<CustomerViewModel> GetCustumersNonOrders();
        IEnumerable<CustomerViewModel> GetCustumersNonOrdersbyEmployees(int id);
        IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValue();
        void RemoveCustomer(string id);
        void EditCustomer(string id, CustomerUpdateModel customerUpdateModel);
        CustomerViewModel AddCustomer(CustomerUpdateModel customerUpdateModel);
        IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValueTotalOrders();
        IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValueTotalOrdersWithDiscount();
        Task TestTransactionAsync();
    }
}

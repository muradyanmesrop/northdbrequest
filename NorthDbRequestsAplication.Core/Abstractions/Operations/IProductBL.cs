﻿using NorthDbRequestsAplication.Core.Db;
using System;
using System.Collections.Generic;
using System.Text;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.UpdateModels;
using NorthDbRequestsAplication.Core.Models.ViewModels;

namespace NorthDbRequestsAplication.Core.Abstractions.Operations
{
    public interface IProductBL
    {
        IEnumerable<ProductViewModel> GetProducts(ProductFilterModel productFilterModel);
        IEnumerable<ProductsCountViewModel> GetCountProductsByCategory();
        IEnumerable<ProductViewModel> GetProductsUnitsLessReorder();
        IEnumerable<ProductViewModel> GetReorderingProducts();
        void RemoveProduct(int id);
        void EditProduct(int id, ProductUpdateModel productUpdateModel);
        ProductViewModel AddProduct(ProductUpdateModel productUpdateModel);
    }
}

﻿using Microsoft.AspNetCore.Http;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models;
using NorthDbRequestsAplication.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NorthDbRequestsAplication.Core.Abstractions.Operations
{
    public interface IUserBL
    {
        Task<UserViewModel> RegisterAsync(UserRegisterModel registerModel, HttpContext httpContext);
        Task LoginAsync(UserLoginModel loginModel, HttpContext httpContext);
        Task LogOutAsync(HttpContext httpContext);
        Task EditRoleAsync(int id, UserUpdateRoleModel userUpdateRoleModel);
    }
}

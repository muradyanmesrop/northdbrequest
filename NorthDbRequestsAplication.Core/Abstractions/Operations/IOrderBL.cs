﻿using System;
using System.Collections.Generic;
using System.Text;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.UpdateModels;
using NorthDbRequestsAplication.Core.Models.ViewModels;

namespace NorthDbRequestsAplication.Core.Abstractions.Operations
{
    public interface IOrderBL
    {
        IEnumerable<OrderViewModel> GetOrders(OrderFilterModel orderFilterModel);
        IEnumerable<OrderHighestFreithViewModel> GetOrdersHighestFreithByCountry(OrderFilterModel orderFilterModel);
        IEnumerable<OrderEmployeeAndProductViewModel> GetSortedOrdersbyEmployeeAndProduct();
        void RemoveOrder(int id);
        void EditOrder(int id, OrderUpdateModel orderUpdateModel);
        OrderViewModel AddOrder(OrderUpdateModel orderUpdateModel);
        IEnumerable<OrderViewModel> GetOrdersEndMonth();
        IEnumerable<OrderCountItemViewModel> GetOrdersWithManyLineItems(OrderFilterModel orderFilterModel);
        IEnumerable<OrderViewModel> GetOrdersRandomAssortment(OrderFilterModel orderFilterModel);
    }
}

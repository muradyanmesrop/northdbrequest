﻿using NorthDbRequestsAplication.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace NorthDbRequestsAplication.Core.Abstractions
{
    public interface IRepositoryManager
    {
        ICustomerRepository Customers { get; }
        IOrderRepository Orders { get; }
        IProductRepository Products { get; }
        IUserRepository Users { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
        ISqlTransaction BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace NorthDbRequestsAplication.Core.Abstractions.Repositories
{
    public interface ISqlRepository <T>
        where T : class
    {
        T Get(int id);
        T Get(string id);
        T Add(T entity);
        void Edit (T entity);
        void Remove(T entity);
        IEnumerable<T> GetWhere(Func<T, bool> predicate);
        int SaveChanges();
        int Count();
    }
}

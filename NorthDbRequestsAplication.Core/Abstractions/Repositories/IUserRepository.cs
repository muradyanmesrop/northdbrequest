﻿using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Abstractions.Repositories
{
    public interface IUserRepository : ISqlRepository<User>
    {
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.ViewModels;

namespace NorthDbRequestsAplication.Core.Abstractions.Repositories
{
    public interface IProductRepository : ISqlRepository<Product>
    {
        IEnumerable<ProductsCountViewModel> GetCountProductsByCategory();
        IEnumerable<ProductViewModel> GetProductsUnitsLessReorder();
        IEnumerable<ProductViewModel> GetReorderingProducts();
    }
}

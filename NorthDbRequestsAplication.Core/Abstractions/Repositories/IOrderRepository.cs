﻿using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.FilterModels;
using NorthDbRequestsAplication.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Abstractions.Repositories
{
    public interface IOrderRepository : ISqlRepository<Order>
    {
        IEnumerable<OrderHighestFreithViewModel> GetOrdersHighestFreith();
        IEnumerable<OrderHighestFreithViewModel> GetOrdersHighestFreithByYear(OrderFilterModel orderFilterModel);
        IEnumerable<OrderHighestFreithViewModel> GetOrdersHighestFreithLastYear(OrderFilterModel orderFilterModel);
        IEnumerable<OrderEmployeeAndProductViewModel> GetSortedOrdersbyEmployeeAndProduct();
        IEnumerable<OrderViewModel> GetOrdersEndMonth();
        IEnumerable<OrderCountItemViewModel> GetOrdersWithManyLineItems(int? count);
        IEnumerable<OrderViewModel> GetOrdersRandomAssortment(int? count);
    }
}

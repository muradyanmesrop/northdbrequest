﻿using NorthDbRequestsAplication.Core.Db;
using NorthDbRequestsAplication.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NorthDbRequestsAplication.Core.Abstractions.Repositories
{
    public interface ICustomerRepository : ISqlRepository<Customer>
    {
        IEnumerable<CustomersCountViewModel> GetCustumersCountByCountryAndCity();
        IEnumerable<CustomerViewModel> GetCustumersNonOrders();
        IEnumerable<CustomerViewModel> GetCustumersNonOrdersbyEmployees(int id);
        IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValue();
        IEnumerable<Customer> GetSortedByRegionCustumers();
        IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValueTotalOrders();
        IEnumerable<CustomerHighValueViewModel> GetSortCustumersbyHighValueTotalOrdersWithDiscount();
    }
}

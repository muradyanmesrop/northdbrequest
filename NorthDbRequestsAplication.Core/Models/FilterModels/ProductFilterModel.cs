﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.FilterModels
{
    public class ProductFilterModel
    {
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? SupplierId { get; set; }
        public int? CategoryId { get; set; }
        public decimal? UnitPriceMore { get; set; }
        public decimal? UnitPriceLess { get; set; }
        public short? UnitsInStockMore { get; set; }
        public short? UnitsInStockLess { get; set; }
        public short? UnitsOnOrderMore { get; set; }
        public short? UnitsOnOrderLess { get; set; }
        public short? ReorderLevelMore { get; set; }
        public short? ReorderLevelLess { get; set; }
        public bool? Discontinued { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.FilterModels
{
    public class OrderFilterModel
    {
        public int? TopCount { get; set; }
        public int? TopCountPersent { get; set; }
        public int? LastMonthCount { get; set; }
        public int? OrderId { get; set; }
        public string CustomerId { get; set; }
        public int? EmployeeId { get; set; }
        public int? OrderYear { get; set; }
        public int? OrderMonth { get; set; }
        public int? RequiredYear { get; set; }
        public int? RequiredMonth { get; set; }
        public int? ShippedYear { get; set; }
        public int? ShippedMonth { get; set; }
        public int? ShipVia { get; set; }
        public decimal? FreightMore { get; set; }
        public decimal? FreightLess { get; set; }
        public string ShipName { get; set; }
        public string ShipAddress { get; set; }
        public string ShipCity { get; set; }
        public string ShipRegion { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }
    }
}

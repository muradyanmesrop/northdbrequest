﻿using System;
using System.Collections.Generic;
using System.Text;
using NorthDbRequestsAplication.Core.Db;

namespace NorthDbRequestsAplication.Core.Models.UpdateModels
{
    public class CustomerUpdateModel
    {
        public string CustomerId { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }

        public static implicit operator Customer( CustomerUpdateModel customerUpdateModel)
        {
            return new Customer
            {
                CustomerId = customerUpdateModel.CustomerId,
                CompanyName = customerUpdateModel.CompanyName,
                ContactName = customerUpdateModel.ContactName,
                ContactTitle = customerUpdateModel.ContactTitle,
                Address = customerUpdateModel.Address,
                City = customerUpdateModel.City,
                Region = customerUpdateModel.Region,
                PostalCode = customerUpdateModel.PostalCode,
                Country = customerUpdateModel.Country,
                Phone = customerUpdateModel.Phone,
                Fax = customerUpdateModel.Fax
            };
        }
    }
}

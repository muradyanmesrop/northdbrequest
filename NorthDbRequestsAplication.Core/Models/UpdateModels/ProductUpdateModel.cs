﻿using NorthDbRequestsAplication.Core.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.UpdateModels
{
    public class ProductUpdateModel
    {
        public string ProductName { get; set; }
        public int? SupplierId { get; set; }
        public int? CategoryId { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal? UnitPrice { get; set; }
        public short? UnitsInStock { get; set; }
        public short? UnitsOnOrder { get; set; }
        public short? ReorderLevel { get; set; }
        public bool Discontinued { get; set; }

        public static implicit operator Product(ProductUpdateModel productUpdateModel)
        {
            return new Product
            {
                ProductName = productUpdateModel.ProductName,
                SupplierId = productUpdateModel.SupplierId,
                CategoryId = productUpdateModel.CategoryId,
                QuantityPerUnit = productUpdateModel.QuantityPerUnit,
                UnitPrice = productUpdateModel.UnitPrice,
                UnitsInStock = productUpdateModel.UnitsInStock,
                UnitsOnOrder = productUpdateModel.UnitsOnOrder,
                ReorderLevel = productUpdateModel.ReorderLevel,
                Discontinued = productUpdateModel.Discontinued
            };
        }
    }
}

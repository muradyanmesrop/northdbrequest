﻿using NorthDbRequestsAplication.Core.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.UpdateModels
{
    public class OrderUpdateModel
    {
        public string CustomerId { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public int? ShipVia { get; set; }
        public decimal? Freight { get; set; }
        public string ShipName { get; set; }
        public string ShipAddress { get; set; }
        public string ShipCity { get; set; }
        public string ShipRegion { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }

        public static implicit operator Order(OrderUpdateModel orderUpdateModel)
        {
            return new Order
            {
                CustomerId = orderUpdateModel.CustomerId,
                EmployeeId = orderUpdateModel.EmployeeId,
                OrderDate = orderUpdateModel.OrderDate,
                RequiredDate = orderUpdateModel.RequiredDate,
                ShippedDate = orderUpdateModel.ShippedDate,
                ShipVia = orderUpdateModel.ShipVia,
                Freight = orderUpdateModel.Freight,
                ShipName = orderUpdateModel.ShipName,
                ShipAddress = orderUpdateModel.ShipAddress,
                ShipCity = orderUpdateModel.ShipCity,
                ShipRegion = orderUpdateModel.ShipRegion,
                ShipPostalCode = orderUpdateModel.ShipPostalCode,
                ShipCountry = orderUpdateModel.ShipPostalCode
            };
        }
    }
}

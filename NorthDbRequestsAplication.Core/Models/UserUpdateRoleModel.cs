﻿using NorthDbRequestsAplication.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models
{
    public class UserUpdateRoleModel
    {
        public Role Role { get; set; }
    }
}

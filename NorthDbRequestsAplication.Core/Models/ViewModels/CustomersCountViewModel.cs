﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.ViewModels
{
    public class CustomersCountViewModel
    {
        public string Country { get; set; }
        public string City { get; set; }
        public int CutomersCount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.ViewModels
{
    public class ProductsCountViewModel
    {
        public string CategoryName { get; set; }
        public int? ProductCount { get; set; }
    }
}

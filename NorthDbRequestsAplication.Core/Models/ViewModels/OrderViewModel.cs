﻿using NorthDbRequestsAplication.Core.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.ViewModels
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {

        }

        public OrderViewModel(Order order)
        {
            OrderId = order.OrderId;
            CustomerId = order.CustomerId;
            EmployeeId = order.EmployeeId;
            OrderDate = order.OrderDate;
            RequiredDate = order.RequiredDate;
            ShippedDate = order.ShippedDate;
            ShipVia = order.ShipVia;
            Freight = order.Freight;
            ShipName = order.ShipName;
            ShipAddress = order.ShipAddress;
            ShipCity = order.ShipCity;
            ShipRegion = order.ShipRegion;
            ShipPostalCode = order.ShipPostalCode;
            ShipCountry = order.ShipCountry;
        }
        public int OrderId { get; set; }
        public string CustomerId { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public int? ShipVia { get; set; }
        public decimal? Freight { get; set; }
        public string ShipName { get; set; }
        public string ShipAddress { get; set; }
        public string ShipCity { get; set; }
        public string ShipRegion { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }

        public static implicit operator OrderViewModel(Order order)
        {
            return new OrderViewModel(order);
        }
    }
}

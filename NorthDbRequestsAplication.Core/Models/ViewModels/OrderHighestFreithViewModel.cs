﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.ViewModels
{
    public class OrderHighestFreithViewModel
    {
        public string ShipCountry { get; set; }
        public decimal? AverrageFreight { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.ViewModels
{
    public class CustomerHighValueViewModel
    {
        public string CustomerId { get; set; }
        public string CompanyName { get; set; }
        public decimal? TotalsWithoutDiscount { get; set; }
        public decimal? TotalsWithDiscount { get; set; }
    }
}

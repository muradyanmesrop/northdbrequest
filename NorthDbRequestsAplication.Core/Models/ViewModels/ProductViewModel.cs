﻿using NorthDbRequestsAplication.Core.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.ViewModels
{
    public class ProductViewModel
    {
        public ProductViewModel()
        {

        }
        public ProductViewModel(Product product)
        {
            ProductId = product.ProductId;
            ProductName = product.ProductName;
            SupplierId = product.SupplierId;
            CategoryId = product.CategoryId;
            QuantityPerUnit = product.QuantityPerUnit;
            UnitPrice = product.UnitPrice;
            UnitsInStock = product.UnitsInStock;
            UnitsOnOrder = product.UnitsOnOrder;
            ReorderLevel = product.ReorderLevel;
            Discontinued = product.Discontinued;
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int? SupplierId { get; set; }
        public int? CategoryId { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal? UnitPrice { get; set; }
        public short? UnitsInStock { get; set; }
        public short? UnitsOnOrder { get; set; }
        public short? ReorderLevel { get; set; }
        public bool? Discontinued { get; set; }

        public static implicit operator ProductViewModel(Product product)
        {
            return new ProductViewModel(product);
        }
    }
}

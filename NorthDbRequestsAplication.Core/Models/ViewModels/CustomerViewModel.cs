﻿using System;
using System.Collections.Generic;
using System.Text;
using NorthDbRequestsAplication.Core.Db;

namespace NorthDbRequestsAplication.Core.Models.ViewModels
{
    public class CustomerViewModel
    {
        public CustomerViewModel()
        {
        }
        public CustomerViewModel(Customer customer)
        {
            CustomerId = customer.CustomerId;
            CompanyName = customer.CompanyName;
            ContactName = customer.ContactName;
            ContactTitle = customer.ContactTitle;
            Address = customer.Address;
            City = customer.City;
            Region = customer.Region;
            PostalCode = customer.PostalCode;
            Country = customer.Country;
            Phone = customer.Phone;
            Fax = customer.Fax;
        }

        public string CustomerId { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }

        public static implicit operator CustomerViewModel(Customer customer)
        {
            return new CustomerViewModel(customer);
        }
    }
}

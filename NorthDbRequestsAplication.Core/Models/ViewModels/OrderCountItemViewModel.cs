﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Models.ViewModels
{
    public class OrderCountItemViewModel
    {
        public int? OrderId { get; set; }
        public int? TotalOrderDetails { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NorthDbRequestsAplication.Core.Db
{
    public partial class NumberEmployeesEachCity
    {
        public string City { get; set; }
        public int? CountEmployees { get; set; }
    }
}

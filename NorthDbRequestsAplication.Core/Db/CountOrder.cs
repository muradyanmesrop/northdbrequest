﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NorthDbRequestsAplication.Core.Db
{
    public partial class CountOrder
    {
        public string ProductName { get; set; }
        public int? QuantityProductOrders { get; set; }
    }
}

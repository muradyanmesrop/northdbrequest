﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NorthDbRequestsAplication.Core.Db
{
    public partial class CurrentProductList
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
    }
}

﻿using NorthDbRequestsAplication.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Db
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
    }
}

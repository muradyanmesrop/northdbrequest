﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Exceptions
{
    public class LogicException : Exception
    {
        public LogicException(string massage) : base(massage)
        {
        }
    }
}

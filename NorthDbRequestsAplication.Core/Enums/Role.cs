﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthDbRequestsAplication.Core.Enums
{
    public enum Role : byte
    {
        Admin,
        User
    }
}
